package com.example.f4t_r4t.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.f4t_r4t.fragment.FirstFragment
import com.example.f4t_r4t.fragment.SecondFragment
import com.example.f4t_r4t.fragment.ThirdFragment

class Adapter (activity: AppCompatActivity): FragmentStateAdapter(activity) {
    override fun getItemCount() = 3





    override fun createFragment(position: Int): Fragment {
        if(position == 0) {
           return FirstFragment()
        }

        if(position == 1){
            return SecondFragment()
        }else{
            return ThirdFragment()
        }







    }
}